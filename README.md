# Orion Load-Test Worker Provisioner

## Requirements: 

- Debian-based Linux system
- AWS creds


## Instructions:

`./launch.sh` will 

- pull deps 
- create ecr in us-east-1
- build the `worker`image
- push the image to ecr
- create VPC and subnets in all requested regions
- provision docker machines in all requested regions
- login to ecr on all docker machines, pull image, and run


Discover docker-machine managed nodes with `docker-machine ls`

Terminate machines with `docker-machine kill <node> && docker-machine rm <node>`


## Notes:

The worker runs against my own test API