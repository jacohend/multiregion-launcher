#!/bin/bash

COMMAND="$1"
AWS_ID="474607191849"
REGIONS=(us-east-1 us-east-2)
NAME="worker"

function deps {
    echo "installing pwgen, jq, and curl..."
    sudo apt-get install -y pwgen jq curl python3-pip
    pip3 install awscli --upgrade
    command -v docker-machine >/dev/null 2>&1 || curl -L https://github.com/docker/machine/releases/download/v0.14.0/docker-machine-`uname -s`-`uname -m` >/tmp/docker-machine && \
    sudo install /tmp/docker-machine /usr/local/bin/docker-machine
    CREATE_ECR_REPO=$(aws ecr create-repository --repository-name $NAME --region us-east-1)
    eval $CREATE_ECR_REPO
}

function build {
    cd worker
    docker build -t $NAME:latest .                                  #  build worker. -f doesnt work with ADD for some reason, using cd
    cd ..
}

function push {
    LOGIN=$(aws ecr get-login --region=us-east-1 --no-include-email)
    eval $LOGIN                                                                 #  login to ecr
    TAG=$(pwgen -A -0 10 1)                                                     #  random hash for tag
    IMAGE=$NAME:latest                                                          
    export ECR_REPO=$AWS_ID.dkr.ecr.us-east-1.amazonaws.com/$NAME:$TAG          #  create current latest ecr image
    docker tag $IMAGE $ECR_REPO
    docker push $ECR_REPO                                                       #  push worker image to ecr
}

function provision {
    aws logs create-log-group --log-group-name $NAME-logs --region $REGION || echo "already created log group"

    VPC_ID=$(aws ec2 create-vpc --cidr-block 10.0.0.0/16 --region $REGION | jq -r ".Vpc.VpcId")                                                         #  create VPC
    aws ec2 modify-vpc-attribute --vpc-id $VPC_ID --enable-dns-support "{\"Value\":true}" --region $REGION
    aws ec2 modify-vpc-attribute --vpc-id $VPC_ID --enable-dns-hostnames "{\"Value\":true}" --region $REGION
    SUBNET_ID=$(aws ec2 create-subnet --vpc-id $VPC_ID --cidr-block 10.0.1.0/24 --region $REGION | jq -r ".Subnet.SubnetId")                            #  create Subnet
    AVAILABILITY_ZONE=$(aws ec2 describe-subnets --subnet-id=$SUBNET_ID --region $REGION | jq -r '.Subnets | .[0].AvailabilityZone' | tail -c 2)        #  get Subnet AZ
    INTERNET_GATEWAY=$(aws ec2 create-internet-gateway --region $REGION | jq -r ".InternetGateway.InternetGatewayId")                                   #  internet access
    aws ec2 attach-internet-gateway --vpc-id $VPC_ID --internet-gateway-id $INTERNET_GATEWAY --region $REGION                                           #  give vpc internet
    ROUTE_TABLE_ID=$(aws ec2 create-route-table --vpc-id $VPC_ID --region $REGION  | jq -r ".RouteTable.RouteTableId") 
    aws ec2 create-route --route-table-id $ROUTE_TABLE_ID --destination-cidr-block 0.0.0.0/0 --gateway-id $INTERNET_GATEWAY --region $REGION 
    aws ec2 associate-route-table  --subnet-id $SUBNET_ID --route-table-id $ROUTE_TABLE_ID --region $REGION                                             #  associate route table with subnet 
    aws ec2 modify-subnet-attribute --subnet-id $SUBNET_ID --map-public-ip-on-launch --region $REGION 
    docker-machine create --driver amazonec2 \
                          --amazonec2-monitoring \
                          --amazonec2-region $REGION \
                          --amazonec2-zone $AVAILABILITY_ZONE \
                          --amazonec2-subnet-id ${SUBNET_ID} \
                          --amazonec2-vpc-id ${VPC_ID} \
                          $NAME-$REGION                                                                                                                 #  docker-machine to provision ec2 instance with cloudwatch monitoring
    docker-machine ssh $NAME-$REGION sudo usermod -a -G docker ubuntu || echo "already added"
    docker-machine ssh $NAME-$REGION sudo gpasswd -a ubuntu docker
    docker-machine restart $NAME-$REGION
    echo "waiting 30 sec for node restart after docker group refresh"
    sleep 30
} 

function run {  
    push                                                                        #  we need to get the ECR_REPO from this
    SERVER_ECR=$(aws ecr get-login --region=us-east-1 --no-include-email)       #  command for server ecr login
    docker-machine ssh $NAME-$REGION $SERVER_ECR                                #  run ecr login command on server
    docker-machine ssh $NAME-$REGION docker pull $ECR_REPO                      #  run docker pull on ecr repo
    docker-machine ssh $NAME-$REGION sudo apt-get install -y python3-pip
    docker-machine ssh $NAME-$REGION pip3 install awscli --upgrade
    docker-machine ssh $NAME-$REGION docker run -d --log-driver=awslogs --log-opt awslogs-region=$REGION --log-opt awslogs-group=$NAME-logs $ECR_REPO   #  log to cloudwatch
}


#  start of program

deps
build

for REGION in ${REGIONS[*]}
do
    provision
    run
done
