import json
import requests
import watchtower, logging, traceback
from time import time


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)
try:
	logger.addHandler(watchtower.CloudWatchLogHandler())
except Exception as e:
	traceback.print_exc()


base_url = 'https://api.sharity.us:8080/api/v1/'
auth_url = base_url + 'auth'
user_url = base_url + 'users/1'
headers = {'Content-type': 'application/json'}
values = {'email': 'jholmer.in@gmail.com',
          'password': 'abcd1234'}

try:
	first_time = time()
	login = requests.post(auth_url, json=values, headers=headers)
	print(login.content)
	access_token = login.json().get('access_token')
	second_time = time()
	logger.info("login latency: {}".format(second_time - first_time))
	logger.info("LOGIN success!")
except Exception as e:
	logger.info("login failure")
	traceback.print_exc()

try:
	first_time = time()
	headers["Authorization"] = "JWT {}".format(access_token)
	user = requests.get(user_url, headers=headers)
	print(user.content)
	logger.info(user.json())
	second_time = time()
	logger.info("user GET latency: {}".format(second_time - first_time))
	logger.info("GET success!")
	print("SUCCESS!")
except Exception as e:
	logger.info("get failure")
	traceback.print_exc()
